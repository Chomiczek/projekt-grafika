
Programowanie Grafiki 3D - OGL Sample

Do skompilwoania i uruchomienia sampli wymagane są:
- system Linuks, lub Windows
- karta graficzna wspierająca OGL 3.3 (4.2 dla sampli >=4) + odpowiednie sterowniki
- cmake w wersji 2.8, lub nowszej
- kompilator wspierający standard C++17 - gcc (8.2, lub nowszy) dla Linuksa, VS (2017, lub nowszy) dla Windowsa
- biblioteka GLFW (min. ver. 3 (wymagana dla Linuksa) - dla Windowsa dostarczona w wersji 3.3)
- biblioteka glbinding - dla Linuksa i Windowsa dostarczona w wersji 3.1)

Biblioteki GLM (0.9.9.6) i GLI (ver. 0.8.3) są dostarczone z samplami
