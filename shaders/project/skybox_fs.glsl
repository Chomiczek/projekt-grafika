#version 420

in vec3 position_ws;

out vec4 frag_color;


layout(binding = 0) uniform samplerCube skybox;




void main()
{

	frag_color = texture(skybox,position_ws);

}
