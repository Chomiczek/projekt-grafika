#version 420

in vec3 position_ws;
in vec3 normal_ws;
in vec2 tex_uv;

out vec4 frag_color;


layout(binding = 0) uniform sampler2D color_tex;

layout(std140, binding = 3) uniform Material
{
    vec3 color;
    float specular_intensity;
    float specular_power;
} material;

layout(std140, binding = 4) uniform AmbientLight
{
    vec3 color;
} ambient_lights;


layout(std140, binding = 5) uniform PointLight
{
	vec3 position_ws;
	float r;
	vec3 color;
	float nothing;
}light;

layout(std140, binding = 11) uniform CameraData
{
    
    vec3 position;
    float gamma; 
} camera;

vec3 ApplyGamma(in vec3 color)
{
	return pow(color,vec3(1.0f/camera.gamma));
}

vec3 GetLight(in vec3 ambient)
{
	vec3 diffuse = vec3(0.f, 0.f, 0.f);
	vec3 specular = vec3(0.f, 0.f, 0.f);
	

	//obliczenia zwiazane z oswietleniem...
    
	vec3 lightDir = normalize(light.position_ws-position_ws);
	float attenuation = 1.0f-clamp(length(light.position_ws-position_ws)/light.r,0.0f,1.0f);
        attenuation*=attenuation;
	vec3 viewDir = normalize(camera.position-position_ws);
	vec3 halfDir = normalize(lightDir+viewDir);
	vec3 norm = normalize(normal_ws);
	diffuse = max(dot(norm, lightDir), 0.0f)*light.color;
	float spec =  pow(max(dot(norm,halfDir),0.0),material.specular_power);
	specular = spec*material.specular_intensity*light.color;
	
	diffuse +=ambient;
 	return clamp(attenuation*diffuse * material.color* texture(color_tex, tex_uv).rgb + specular, 0.f, 1.f);
}

void main()
{
	vec3 result = GetLight(ambient_lights.color);
		
	frag_color = vec4(result, 1.f);
	//frag_color = vec4(ApplyGamma(result), 1.f);
}










