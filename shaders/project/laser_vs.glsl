#version 420

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec2 vertex_tex_uv;
layout(location = 2) in vec3 vertex_normal;



layout(std140, binding = 1) uniform Model
{
    mat4x4 model_matrix;
} model;


layout(std140, binding = 10) uniform CameraMat
{
    mat4x4 view;
    mat4x4 projection;
} camera;


void main()
{
	gl_Position =  camera.projection * camera.view *model.model_matrix * vec4(vertex_position, 1.f);
}
