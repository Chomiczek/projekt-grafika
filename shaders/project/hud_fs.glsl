#version 420

out vec4 frag_color;


in float mag_color;


void main()
{
	frag_color = vec4(.5f-mag_color, mag_color,0.0f,1.f);
}
