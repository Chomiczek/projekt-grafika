#version 420

out vec4 frag_color;

layout(std140, binding = 6) uniform LaserLight
{
	vec3 color;
} laser;




void main()
{
	frag_color = vec4(laser.color,1.f);
}
