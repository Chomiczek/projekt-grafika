#version 420

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec2 vertex_tex_uv;
layout(location = 2) in vec3 vertex_normal;

out vec3 position_ws;

layout(std140, binding = 10) uniform CameraMat
{
    mat4x4 view;
    mat4x4 projection;
} camera;


void main()
{
	position_ws = vertex_position;
	gl_Position = camera.projection * mat4x4(mat3(camera.view)) * vec4(vertex_position,1.0f);
}
