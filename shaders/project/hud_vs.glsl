#version 420

#define SIZE 6

layout(location = 0) in vec3 vertex_position;

out float mag_color;

layout(std140, binding = 7) uniform HUDModel
{
    mat4x4 model_matrix[SIZE];
} model;

layout(std140, binding = 8) uniform Mag
{
	vec4 status[SIZE];
} mag;


layout(std140, binding = 10) uniform CameraMat
{
    mat4x4 view;
    mat4x4 projection;
} camera;


void main()
{
	gl_Position =  camera.projection * model.model_matrix[gl_InstanceID] * vec4(vertex_position, 1.f);
	//mag_color= vec3(0.0f,mag.status[2],0.0f);
	mag_color = mag.status[gl_InstanceID].x;
}
