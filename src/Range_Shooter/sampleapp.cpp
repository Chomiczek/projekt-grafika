#include "sampleapp.h"
using namespace OGLA;

BaseApp::BaseApp() : OGLA::OGLApplication(1366u, 768u, "Range Shooter", 4u, 2u), camera(glm::vec2(1366u, 768u))
{
}

BaseApp::~BaseApp()
{
}

void BaseApp::addObjectToScene(Object* obj)
{
   objectsOnScene.push_back(obj);

}

void BaseApp::addLightToScene(Light* obj)
{
    lightOnScene.push_back(obj);

}


void BaseApp::mouseScrollCallback(double xoffset, double yoffset)
{
    camera.mouseScrollCallback(xoffset, yoffset);
    for each (Object * obj in objectsOnScene)
    {
        obj->mouseScrollCallback(xoffset, yoffset);
    }

    for each (Object * obj in lightOnScene)
    {
        obj->mouseScrollCallback(xoffset, yoffset);
    }
    
}

void BaseApp::reshapeCallback(std::uint16_t width, std::uint16_t height)
{
    camera.reshapeCallback(width,height);
    for each (Object * obj in objectsOnScene)
    {
        obj->reshapeCallback(width, height);
    }
    for each (Object * obj in lightOnScene)
    {
        obj->reshapeCallback(width, height);
    }
    gl::glViewport(0, 0, width, height);
}

void BaseApp::keyCallback(int key, int scancode, int action, int mods)
{
    camera.keyCallback(key, scancode, action, mods);
    for each (Object * obj in objectsOnScene)
    {
        obj->keyCallback(key, scancode, action, mods);
    }
    for each (Object * obj in lightOnScene)
    {
        obj->keyCallback(key, scancode, action, mods);
    }
    
}

void BaseApp::cursorPosCallback(double xpos, double ypos)
{
    camera.cursorPosCallback(xpos, ypos);
    for each (Object * obj in objectsOnScene)
    {
        obj->cursorPosCallback(xpos, ypos);
    }
    for each (Object * obj in lightOnScene)
    {
        obj->cursorPosCallback(xpos, ypos);
    }
}

void BaseApp::mouseButtonCallback(int button, int action, int mods)
{
    camera.mouseButtonCallback(button,action,mods);
    for each (Object * obj in objectsOnScene)
    {
        obj->mouseButtonCallback(button, action, mods);
    }
    for each (Object * obj in lightOnScene)
    {
        obj->mouseButtonCallback(button, action, mods);
    }
    
}

void BaseApp::setUniforms(int shaderIndex, std::vector<UniformData> uniformBindings)
{
    gl::GLuint handle=-1;
    for each (UniformData data in uniformBindings)
    {
        for each (Shader program in shaderPrograms)
        {
            if ((handle = program.getUniformHandle(data.name)) != -1)
            {
                break;
            }
        }
        if(handle!=-1 ) shaderPrograms[shaderIndex].setUniform(data.name, handle);
        else shaderPrograms[shaderIndex].setUniform(data.name, data.binding_index, data.data_size);
        
    }
}


void BaseApp::loadShaders()
{
#pragma region Shader 0: Project + Single Light
    std::string vs_path = "../../../shaders/project/project_vs.glsl";
    std::string fs_path = "../../../shaders/project/single_light_fs.glsl";


    shaderPrograms.push_back(Shader(vs_path, fs_path));

    std::vector<UniformData> uniformBindings;
    uniformBindings.push_back({ "Model", 1u,sizeof(glm::mat4x4) });
    uniformBindings.push_back({ "Material", 3u,sizeof(Object::Material) });
    uniformBindings.push_back({ "AmbientLight", 4u, sizeof(glm::vec3) });
    uniformBindings.push_back({ "PointLight", 5u, sizeof(Light::Point_Light) });

    uniformBindings.push_back({ "CameraMat", 10u,2 * sizeof(glm::mat4x4) });
    uniformBindings.push_back({ "CameraData", 11u,sizeof(glm::vec4) });
    setUniforms(0, uniformBindings);
#pragma endregion

#pragma region Shader 1: Skybox
    std::string skybox_vs_path = "../../../shaders/project/skybox_vs.glsl";
    std::string skybox_fs_path = "../../../shaders/project/skybox_fs.glsl";

    shaderPrograms.push_back(Shader(skybox_vs_path, skybox_fs_path));

    uniformBindings.clear();
    uniformBindings.push_back({ "CameraMat", 10u,2 * sizeof(glm::mat4x4) });

    setUniforms(1, uniformBindings);
#pragma endregion

#pragma region Shader 2: Laser
    std::string laser_vs_path = "../../../shaders/project/laser_vs.glsl";
    std::string laser_fs_path = "../../../shaders/project/laser_2_fs.glsl";
    shaderPrograms.push_back(Shader(laser_vs_path, laser_fs_path));

    uniformBindings.clear();
    uniformBindings.push_back({ "Model", 1u, sizeof(glm::mat4x4) });
    uniformBindings.push_back({ "CameraMat", 10u, 2 * sizeof(glm::mat4x4) });
    uniformBindings.push_back({ "LaserLight", 6u, sizeof(glm::vec3) });
    setUniforms(2, uniformBindings);
#pragma endregion

#pragma region Shader 3: HUD
    std::string hud_vs_path = "../../../shaders/project/hud_vs.glsl";
    std::string hud_fs_path = "../../../shaders/project/hud_fs.glsl";
    shaderPrograms.push_back(Shader(hud_vs_path, hud_fs_path));

    uniformBindings.clear();
    uniformBindings.push_back({ "HUDModel", 7u, BAR_SIZE* sizeof(glm::mat4x4) });
    uniformBindings.push_back({ "CameraMat", 10u, 2 * sizeof(glm::mat4x4) });
    uniformBindings.push_back({ "Mag", 8u, BAR_SIZE * sizeof(glm::vec4) });
    setUniforms(3, uniformBindings);
#pragma endregion

}

void BaseApp::addStaticLights()
{
    addLightToScene(&ambietLight);
    for (int i = 0; i < LIGHT_NUM; i++)
    {
        addLightToScene(&lightPoint[i]);
    }

}

void BaseApp::setTargets()
{
    std::string model_name = "../../../data/Target/source/Mark_orig.obj";
    std::string texture_path = "../../../data/Target/textures/Mark_low_DefaultMaterial_BaseColor2.dds";
    float angle = glm::pi<float>()*2 / TARGET_NUM;
    float distance = 40;
   
    for (int i = 0; i < TARGET_NUM; i++)
    {
        targets[i].setModelPath(model_name);
        targets[i].setTexture(texture_path);
        targets[i].setShader(shaderPrograms[0]);
        addObjectToScene(&targets[i]);
        
    }

   
    targets[0].setUpMatrix(glm::mat4x4(1.f));
    targets[0].setPosition(glm::vec3(0.f, 2.f, -distance));
    targets[1].setUpMatrix(OGLA::rotationMatrix(glm::pi<float>() - angle, camera.getUpDirection()));
    targets[1].setPosition(glm::vec3(10.f, 2.f, -distance));
    targets[2].setUpMatrix(OGLA::rotationMatrix(glm::pi<float>() + angle, camera.getUpDirection()));
    targets[2].setPosition(glm::vec3(-10.f, 2.f, -distance));
    targets[3].setUpMatrix(glm::mat4x4(1.f));
    targets[3].setPosition(glm::vec3(-5.f, 2.f, -1.5*distance));
    targets[4].setUpMatrix(OGLA::rotationMatrix(glm::pi<float>() - angle, camera.getUpDirection()));
    targets[4].setPosition(glm::vec3(7.f, 2.f, -1.5*distance));
    targets[5].setUpMatrix(OGLA::rotationMatrix(glm::pi<float>() + angle, camera.getUpDirection()));
    targets[5].setPosition(glm::vec3(2.f, 2.f, -1.7*distance));


}

void BaseApp::setLights()
{


    ambietLight.setShader(shaderPrograms[0]);
    ambietLight.setAmbientLight(glm::vec3(.3f, .0f, .0f));
    addLightToScene(&ambietLight);

    for (int i = 0; i < LIGHT_NUM; i++)
    {
        lightPoint[i].setShader(shaderPrograms[0]);
        addLightToScene(&lightPoint[i]);
    }
    
    lightPoint[0].setPointLight(glm::vec3(0.0f, 0.f, 0.f), 200.f, glm::vec3(.3f, 0.0f, .0f));
    lightPoint[1].setPointLight(glm::vec3(0.0f, 4.f, -19.f), 20.f, glm::vec3(.1f, 0.1f, .1f));
   
    lightPoint[2].setPointLight(glm::vec3(-3.0f, 3.f, -2.f), 40.f, glm::vec3(.0f, 0.1f, .8f));
    lightPoint[3].setPointLight(glm::vec3(10.0f, 3.f, -2.f), 120.f, glm::vec3(0.f, 0.f, .6f));


}

bool BaseApp::init(void)
{
    std::cout << "Init..." << std::endl;

    // ustalamy domyślny kolor ekranu
    gl::glClearColor(0.f, 0.f, 0.f, 1.f);

    // wlaczmy renderowanie tylko jednej strony poligon-ow
    gl::glEnable(gl::GL_CULL_FACE);
    // ustalamy, ktora strona jest "przodem"
    gl::glFrontFace(gl::GL_CCW);
    // ustalamy, ktorej strony nie bedziemy renderowac
    gl::glCullFace(gl::GL_BACK);

    gl::glEnable(gl::GL_DEPTH_TEST);
    gl::glDepthMask(gl::GL_TRUE);
    gl::glEnable(gl::GL_BLEND);
    gl::glBlendFunc(gl::GL_ONE, gl::GL_ONE);

    gl::glDepthFunc(gl::GL_LEQUAL);

   

    loadShaders();


#pragma region Skybox


    skybox.setShader(shaderPrograms[1]);
    skybox.setCubeTexture("../../../data/skybox/city_cubemap.dds");
    
    
#pragma endregion



   
    setTargets();




    pyramid.setPosition(glm::vec3(0.f, 0.f,17.f));
    pyramid.setShader(shaderPrograms[0]);
    addObjectToScene(&pyramid);


    std::string model_name = "../../../data/DC-15S/source/DC-15S.obj";
    std::string texture_path = "../../../data/DC-15S/textures/DC-15S_Base_Color.dds";
    gun.setModelPath(model_name);
    gun.setTexture(texture_path);
    gun.setCamera(&camera);
    gun.setShader(shaderPrograms[0], shaderPrograms[2], shaderPrograms[0]);
    addObjectToScene(&gun);



    setLights();
    

    floor.setShader(shaderPrograms[0]);
    floor.setPosition(glm::vec3(0.f, -10.f, 0.0f));
    addObjectToScene(&floor);

    hud.setShader(shaderPrograms[3]);
    hud.setGun(&gun);
    hud.setCamera(&camera);
    addObjectToScene(&hud);


    camera.init(shaderPrograms);
    for each (Light * obj in lightOnScene)
    {
      
        obj->init();
    }


    for each (Object* obj in objectsOnScene)
    { 
        
        obj->init();
    }
    skybox.init();
    std::cout << "Static Lights on scene: "<<lightOnScene.size() << std::endl;

    
    
   
    return true;
}

bool BaseApp::frame(float delta_time)
{
    //std::cout << "Frame time:\t" << delta_time<< std::endl;
    lightOnScene.clear();
    addStaticLights();
   
    
    for each (LaserBeam * laser in gun.getLaserPtrs())
    {

        addLightToScene(laser->getLightPtr());
        
        laser->setHit(Collider::reycastOnAll(objectsOnScene, laser->getPosition(), laser->getDirection()));
        
        
    }


    camera.frame(delta_time, shaderPrograms, lightOnScene.size());
    for each (Object* obj in objectsOnScene)
    {
       
       
        obj->frame(delta_time);
        
       
    }


    for each (Light * light in lightOnScene)
    {
        light->frame(delta_time);
    }

    
    skybox.frame(delta_time);
    
    

    
    camera.draw();
    for each (Light* light in lightOnScene)
    {
        
        light->draw();
        
        for each (Object* obj in objectsOnScene)
        {
            
            obj->draw();
        }
       
    }
   
    skybox.draw();
   
    return true;
}

void BaseApp::release(void)
{
   
    camera.release();
    for each (Object * obj in objectsOnScene)
    {
        obj->release();
    }
    for each (Light * light in lightOnScene)
    {
        light->release();
    }

    skybox.release();
}
