#pragma once
#include "libs.h"
#include "shader.h"
#include "tiny_obj_loader.h"


class Object
{
#pragma region Structures
public:
		// Structure containing all properties for object material
		struct Material
		{
			glm::vec3 color;
			float specular_intensity;
			float specular_power;
		};

		// Structure for object shape
		struct Shape
		{
			std::vector<gl::GLfloat> vertices;
			std::vector<gl::GLushort> indices;
		};

#pragma endregion

protected:
		// Number of indices to be drawn in glDraw (in frame)
		int indicesNumber;

		// Shape containing all information needed for collision detection
		Shape shapeCollider;
		Shader shaderProgram;

		gl::GLuint objectVAO;
		gl::GLuint objectTexture;
		Material objectMaterial;

protected: 

		// Draws object within vao hit scene
		void drawOnScreen()
		{
			gl::glBindVertexArray(objectVAO);
			gl::glDrawElements(gl::GL_TRIANGLES, indicesNumber, gl::GL_UNSIGNED_SHORT, nullptr);
			gl::glBindVertexArray(0u);
		}

		// Updates material information in shader uniform 
		void Object::updateMaterialUniform(std::string uniform_name = "Material"){ shaderProgram.updateUniform<Material>(uniform_name, &objectMaterial, sizeof(Material)); }

#pragma region Constructors & Destructors	

public:
		Object() : objectVAO(0u), objectTexture(0u), indicesNumber(0) {}
		virtual ~Object() { this->release(); }
#pragma endregion

#pragma region Get Methods

public:
		virtual glm::vec3 getPosition(){ return glm::vec3();}

		virtual glm::mat4x4 getMatrix(){ return glm::mat4x4();}

		virtual Shape getShape(){ return shapeCollider;}

		virtual Shape* getShapePtr() { return &shapeCollider; }

		virtual bool getIsHit() { return false; }
#pragma endregion
		
#pragma region Set Methods

protected:
	
	// Sets shape for the object collider
	virtual void setShape(std::vector<gl::GLfloat> vertices, std::vector<gl::GLushort> indices)
	{
		shapeCollider.indices.clear();
		shapeCollider.vertices.clear();
		shapeCollider.vertices = vertices;
		shapeCollider.indices = indices;

	}

public:
		// Setting shader for object
		virtual void setShader(Shader shader) {shaderProgram = shader;}
		
		// Sets Material for this object
		virtual void setMaterial(glm::vec3 material_color, float specular_intensity, float specular_power)
		{
			//std::cout << "\t# " << typeid(*this).name() << ": Generating Material ..." << std::endl;
			objectMaterial = { material_color,specular_intensity,specular_power };

		}

		virtual void setHit(){}
#pragma endregion

#pragma region Object Generation
public:
		// Loading textures for object
		virtual gl::GLuint loadTexture(std::string path) 
		{
			//std::cout << "\t# " << typeid(*this).name() << ": Loading Texture ..." << std::endl;
			if (auto load_tex_result = OGLA::loadTextureDDS(path))
			{
				objectTexture = load_tex_result.value();
				return objectTexture;
			}
			else
			{
				std::cerr << "\t# " << typeid(*this).name() << ": Error - can't load texture from path: " << path << std::endl;
				return NULL;
			}

		}

		virtual gl::GLuint generateVBO(std::vector<gl::GLfloat> vertices)
		{
			gl::GLuint vbo_handle;
			gl::glGenBuffers(1, &vbo_handle);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo_handle);
			gl::glBufferData(gl::GL_ARRAY_BUFFER, vertices.size() * sizeof(gl::GLfloat), vertices.data(), gl::GL_STATIC_DRAW);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
			return vbo_handle;
		}

		virtual gl::GLuint generateIndexBuffer(std::vector<gl::GLushort> indices)
		{
			gl::GLuint index_buffer_handle;
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, objectVAO);
			gl::glGenBuffers(1, &index_buffer_handle);
			gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, index_buffer_handle);
			gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(gl::GLushort), indices.data(), gl::GL_STATIC_DRAW);
			gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
			return index_buffer_handle;
		}

		virtual void generateVAO(gl::GLuint vbo_handle, gl::GLuint index_buffer_handle, const gl::GLuint vertex_position_loction, const gl::GLuint vertex_tex_uv_loction, const gl::GLuint vertex_normal_loction)
		{
			
			gl::glGenVertexArrays(1, &objectVAO);
			gl::glBindVertexArray(objectVAO);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo_handle);

			gl::glVertexAttribPointer(vertex_position_loction, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 8, nullptr);
			gl::glEnableVertexAttribArray(vertex_position_loction);

			gl::glVertexAttribPointer(vertex_tex_uv_loction, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 8, reinterpret_cast<const gl::GLvoid*>(sizeof(float) * 3));
			gl::glEnableVertexAttribArray(vertex_tex_uv_loction);

			gl::glVertexAttribPointer(vertex_normal_loction, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 8, reinterpret_cast<const gl::GLvoid*>(sizeof(float) * 5));
			gl::glEnableVertexAttribArray(vertex_normal_loction);

			gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, index_buffer_handle);

			gl::glBindVertexArray(0u);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
		}
		
		// Generating object based hit given vertices and indices
		virtual gl::GLuint generateObject(std::vector<gl::GLfloat> vertices, std::vector<gl::GLushort> indices,
			const gl::GLuint position_loction = 0u, const gl::GLuint tex_uv_loction = 1u, const gl::GLuint normal_loction = 2u)
		{

			//std::cout << "\t# " << typeid(*this).name() << ": Generating buffers..." << std::endl;
			gl::GLuint vbo_handle = generateVBO(vertices);

			gl::GLuint index_buffer_handle = generateIndexBuffer(indices);

			generateVAO(vbo_handle, index_buffer_handle,position_loction, tex_uv_loction, normal_loction);

			gl::glBindVertexArray(0u);
			gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
			gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);

			if (vbo_handle)
			{

				gl::glDeleteBuffers(1, &vbo_handle);
				vbo_handle = 0u;
			}
			if (index_buffer_handle)
			{

				gl::glDeleteBuffers(1, &index_buffer_handle);
				index_buffer_handle = 0u;
			}

			return objectVAO;
		}

		// Creates 3D Cylinder Shape with set coordinates, texture uv and normals based hit gives sizes and sides number
		// uv_multiplayer allows resizing textures uv to fit more or less texture in object
		virtual Shape create3DCylinder(float height, float radius, int sides_number, float uv_multiplyer = 1.0f)
		{
			Shape cylinder;


			std::vector<gl::GLfloat> base;

			float sector_angle = 2 * glm::pi<float>() / (float)sides_number;

			for (int i = 0; i < sides_number; ++i)
			{
				base.push_back(glm::cos(i * sector_angle));
				base.push_back(glm::sin(i * sector_angle));
				base.push_back(0);
			}


#pragma region Sides Setup


			float h;
			float x, y, z;

			for (int i = 0; i < 2; i++)
			{
				h = -height / 2.0f + i * height;
				for (int j = 0, k = 0; j < sides_number; ++j, k += 3)
				{

					x = base[k];
					y = base[k + 1];
					z = base[k];

					//Coordinates
					cylinder.vertices.push_back(x * radius);
					cylinder.vertices.push_back(y * radius);
					cylinder.vertices.push_back(h);

					//UV
					cylinder.vertices.push_back(((float)j / sides_number) * uv_multiplyer);
					cylinder.vertices.push_back((1.0f - i) * uv_multiplyer);

					//Normals
					cylinder.vertices.push_back(x);
					cylinder.vertices.push_back(y);
					cylinder.vertices.push_back(z);
				}

			}

#pragma endregion

			int baseCenterIndex = (int)cylinder.vertices.size() / 8;
			int topCenterIndex = baseCenterIndex + sides_number + 1;

			float nz;
#pragma region Base & Top Setup

			for (int i = 0; i < 2; ++i)
			{
				h = -height / 2.0f + i * height;
				nz = -1 + i * 2;

				// CenterPoint
				cylinder.vertices.push_back(0);
				cylinder.vertices.push_back(0);
				cylinder.vertices.push_back(h);

				cylinder.vertices.push_back(0.5f * uv_multiplyer);
				cylinder.vertices.push_back(0.5f * uv_multiplyer);

				cylinder.vertices.push_back(0);
				cylinder.vertices.push_back(0);
				cylinder.vertices.push_back(nz);

				for (int j = 0, k = 0; j < sides_number; ++j, k += 3)
				{
					x = base[k];
					y = base[k + 1];

					//Coordinates
					cylinder.vertices.push_back(x * radius);
					cylinder.vertices.push_back(y * radius);
					cylinder.vertices.push_back(h);

					//UV
					cylinder.vertices.push_back((-x * 0.5f + 0.5f) * uv_multiplyer);
					cylinder.vertices.push_back((-y * 0.5f + 0.5f) * uv_multiplyer);

					//Normals
					cylinder.vertices.push_back(x);
					cylinder.vertices.push_back(y);
					cylinder.vertices.push_back(nz);
				}
			}

#pragma endregion


			int k1 = 0;
			int k2 = sides_number;

			for (int i = 0; i < sides_number; ++i, ++k1, ++k2)
			{
				cylinder.indices.push_back(k1);
				cylinder.indices.push_back(k1 + 1);
				cylinder.indices.push_back(k2);
				if (i + 1 != sides_number) {
					cylinder.indices.push_back(k2);
					cylinder.indices.push_back(k1 + 1);
					cylinder.indices.push_back(k2 + 1);
				}
				else
				{


					cylinder.indices.push_back(0);
					cylinder.indices.push_back(k1 + 1);
					cylinder.indices.push_back(k1);
				}
			}


			for (int i = 0, k = baseCenterIndex + 1; i < sides_number; i++, k++)
			{
				if (i < sides_number - 1)
				{
					cylinder.indices.push_back(baseCenterIndex);
					cylinder.indices.push_back(k + 1);
					cylinder.indices.push_back(k);
				}
				else
				{
					cylinder.indices.push_back(baseCenterIndex);
					cylinder.indices.push_back(baseCenterIndex + 1);
					cylinder.indices.push_back(k);
				}

			}

			for (int i = 0, k = topCenterIndex + 1; i < sides_number; i++, k++)
			{
				if (i < sides_number - 1)
				{
					cylinder.indices.push_back(topCenterIndex);
					cylinder.indices.push_back(k);
					cylinder.indices.push_back(k + 1);
				}
				else
				{
					cylinder.indices.push_back(topCenterIndex);
					cylinder.indices.push_back(k);
					cylinder.indices.push_back(topCenterIndex + 1);
				}

			}

			return cylinder;
		}

#pragma endregion
		
#pragma region Callbacks
public:
		// Callback that activates after windows reshape
		virtual void reshapeCallback(std::uint16_t width, std::uint16_t height){}
		// Callback that activates after using mouse scroll
		virtual void mouseScrollCallback(double xoffset, double yoffset){}
		// Callback that activates after keyboard key press
		virtual void keyCallback(int key, int scancode, int action, int mods){}
		// Callback that activates after mouse position change
		virtual void cursorPosCallback(double xpos, double ypos){}
		// Callback that activates after mouse button press
		virtual void mouseButtonCallback(int button, int action, int mods){}

#pragma endregion

public:

		// Method called after start of application and OGL initialization 
		virtual bool init() { return false; }

		// Method called each frame to update parameters
		virtual bool frame(float delta_time) { return false; }

		// Method called each frame to draw object hit screen
		virtual bool draw() { return false; }

		// Method called before closing application 
		virtual void release()
		{
			gl::glBindVertexArray(0);
			if (objectVAO)
			{
				gl::glDeleteVertexArrays(1, &objectVAO);
				objectVAO = 0u;

			}
			gl::glUseProgram(0);

		}
};
