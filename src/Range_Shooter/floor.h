#pragma once
#include "libs.h"


class Floor: public Object
{
protected:
	gl::GLuint sampler_handle;


	glm::vec3 model_position = glm::vec3(0.f, 0.f, 0.f);

	glm::mat4x4 model_matrix;


public:
	Floor();
	virtual ~Floor();

	virtual glm::vec3 getPosition();
	virtual void setPosition(glm::vec3 new_position);

	virtual glm::mat4x4 getMatrix();
	

	
	virtual bool init();

	virtual bool draw();


};
