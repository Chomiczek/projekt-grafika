#pragma once
#include "libs.h"


class Light : public Object
{
public: 
	struct Point_Light {
		glm::vec3 position;
		float radius;
		glm::vec3 color;
		float nothing;
	};

protected:



public:
	Light(){}
	virtual ~Light(){}


};