#include "camera.h"

using namespace OGLA;

glm::vec3 Camera::getMovementDirection(glm::vec3 camera_forward)
{
	auto direction = glm::vec3(0.f, 0.f, 0.f);
	auto forward = glm::normalize(camera_forward);
	auto right = glm::normalize(glm::cross(camera_forward, up_direction));

	if (wsadqe_state.any())
	{
		direction += forward * static_cast<float>(wsadqe_state[0] - wsadqe_state[1]);
		direction += right * static_cast<float>(wsadqe_state[3] - wsadqe_state[2]);
		direction += up_direction * static_cast<float>(wsadqe_state[4] - wsadqe_state[5]);

		if (glm::length(direction) > 0) { direction = glm::normalize(direction); }
	}

	return direction;
}


glm::vec3 Camera::updateCameraTarget(glm::vec2 cursor_position)
{
	//check if LMB is currently pressed
	//if (!lmb_down) { return camera_target; }

	//calculate normalized cursor shift
	auto normalized_cursor_shift = (cursor_position - previous_cursor_position) / window_size;

	//rotate camera direction based hit cursor shift
	auto camera_direction = camera_target - camera_position;
	auto right_direction = glm::normalize(glm::cross(camera_direction, up_direction));
	auto rotation = OGLA::rotationMatrix(normalized_cursor_shift.x * camera_speed, up_direction) * rotationMatrix(normalized_cursor_shift.y * camera_speed, right_direction);
	auto unifrom_camera_direction = glm::vec4(camera_direction, 0.f) * rotation;

	//apply camera direction to camera position to get current camera target
	return camera_position + glm::vec3(unifrom_camera_direction);
}

Camera::Camera():gammaValue(2.2f), previous_cursor_position(glm::vec2()), projection_matrix(glm::mat4x4()), view_matrix(glm::mat4x4())
{}

void Camera::setGamma(float gamma)
{
	if (gamma <= 0) return;
	gammaValue = gamma;
}

glm::vec3 Camera::getPosition()
{
	return camera_position;
}

glm::vec3 Camera::getDirection()
{
	return camera_target;
}

glm::vec3 Camera::getUpDirection()
{
	return up_direction;
}

Camera::Camera(glm::vec2 window_size)
{
	this->window_size = window_size;
	Camera();
}

Camera::~Camera()
{
}

bool Camera::isCameraReady()
{
	return cameraReady;
}

glm::mat4x4 Camera::getProjectionMatrix()
{
	if (isCameraReady())	return projection_matrix;
	std::cout << "\t# " << typeid(*this).name() << ": WARNING Camera is not ready! Call Init first! . . . " << std::endl;
	return glm::mat4x4();
}

glm::mat4x4 Camera::getViewMatrix()
{
	if (isCameraReady())	return view_matrix;
	std::cout << "\t# " << typeid(*this).name() << ": WARNING Camera is not ready! Call Init first! . . . " << std::endl;
	return glm::mat4x4();
}

void Camera::reshapeCallback(std::uint16_t width, std::uint16_t height)
{
	gl::glViewport(0, 0, width, height);
	projection_matrix = glm::perspective(glm::radians<float>(60.f), width / static_cast<float>(height), 0.01f, 100.f);
	window_size = glm::vec2(width, height);
}

void Camera::keyCallback(int key, int scancode, int action, int mods)
{
	if (key == 32 && action == 1)
	{
		allowMovement = !allowMovement;
		if (allowMovement) std::cout << "Movement allowed . . ." << std::endl;
		else std::cout << "Movement forbidden . . ." << std::endl;

	}
	if (!allowMovement) return;
	switch (key) {
	case 87:
		if (action == 1) { wsadqe_state.set(0); }
		else if (action == 0) { wsadqe_state.reset(0); }
		break;
	case 83:
		if (action == 1) { wsadqe_state.set(1); }
		else if (action == 0) { wsadqe_state.reset(1); }
		break;
	case 65:
		if (action == 1) { wsadqe_state.set(2); }
		else if (action == 0) { wsadqe_state.reset(2); }
		break;
	case 68:
		if (action == 1) { wsadqe_state.set(3); }
		else if (action == 0) { wsadqe_state.reset(3); }
		break;
	case 81: break;
		if (action == 1) { wsadqe_state.set(4); }
		else if (action == 0) { wsadqe_state.reset(4); }
		break;
	case 69: break;
		if (action == 1) { wsadqe_state.set(5); }
		else if (action == 0) { wsadqe_state.reset(5); }
		break;
	default:
		break;
	}
}

void Camera::cursorPosCallback(double xpos, double ypos)
{
	camera_target = updateCameraTarget(glm::vec2(xpos, ypos));

	previous_cursor_position = glm::vec2(xpos, ypos);
}

void Camera::mouseButtonCallback(int button, int action, int mods)
{
	if (button == 0) { lmb_down = static_cast<bool>(action); }
}

bool Camera::init(std::vector<Shader> shaderPrograms)
{
	//std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;
	
#pragma region Class Field Initialization
	// inicjalizacja p�l klasy
	camera_position = glm::vec3(0.0f, 0.0f, 0.0f);
	view_matrix = glm::lookAt(camera_position, camera_target, up_direction);
	projection_matrix = glm::perspective(glm::radians<float>(60.f), window_size.x / static_cast<float>(window_size.y), 0.01f, 10000.f);
	
#pragma endregion
	

	cameraReady = true;
	return true;
}

bool Camera::frame(float delta_time, std::vector<Shader> shaderPrograms, int lightNumber)
{
	if (!isCameraReady())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING Camera is not ready! Call Init first! . . . " << std::endl;
		return false;
	}
	// update pozycji i targetu kamery
	auto movement = getMovementDirection(camera_target - camera_position) * delta_time * camera_speed;
	camera_position += movement;
	camera_target += movement;
	view_matrix = glm::lookAt(camera_position, camera_target, up_direction);
	//projection_matrix = glm::perspective(glm::radians<float>(60.f), window_size.x / static_cast<float>(window_size.y), 0.01f, 100.f);
	std::array<glm::mat4x4, 2u> matrices = { view_matrix, projection_matrix };
	glm::vec4 cameraData = glm::vec4(camera_position,1.0f/* gammaValue/static_cast<float>(lightNumber)*/);
	
	for each (Shader program in shaderPrograms)
	{
		program.use();

		program.updateUniform< std::array<glm::mat4x4, 2u>>("CameraMat", &matrices, matrices.size() * sizeof(glm::mat4x4), true);


		program.updateUniform<glm::vec4>("CameraData", &cameraData, sizeof(glm::vec4), true);
	}

	return true;
}
