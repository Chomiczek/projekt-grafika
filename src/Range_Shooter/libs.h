#pragma once

#include "../libs/OGLAppFramework/oglappframework.h"
#include "../libs/OGLAppFramework/utilities.h"

#include <iostream>
#include <string>
#include <optional>
#include <memory>
#include <cstring>
#include <string>
#include <array>
#include <vector>
#include <bitset>
#include <map>

#include "object.h"
#include "shader.h"









struct UniformData
{
	const std::string name;
	const gl::GLuint binding_index;
	const gl::GLsizeiptr data_size;

};