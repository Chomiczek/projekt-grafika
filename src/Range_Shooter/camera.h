#pragma once

#include "libs.h"


class Camera : public Object
{
protected:

	bool cameraReady = false;
	bool allowMovement = false;
	float gammaValue;
	glm::vec3 up_direction = glm::vec3(0.f, 1.f, 0.f);
	glm::vec3 camera_position = glm::vec3(0.f, 0.f, 0.f);
	glm::vec3 camera_target = glm::vec3(0.f, 0.f, -1.f);

	
	glm::mat4x4 view_matrix;
	glm::mat4x4 projection_matrix;

	float rotation_angle = 0.f;
	float rotation_speed = 2.f;

	glm::vec2 window_size = glm::vec2(1366, 768);
	bool lmb_down = false;
	glm::vec2 previous_cursor_position;
	float cursor_speed = 10.f;

	std::bitset<6> wsadqe_state;
	float camera_speed = 6.f;


	// Method that calculate camera shift based hit keyboard input
	virtual glm::vec3 getMovementDirection(glm::vec3 camera_forward);
	
	Camera();
public:
	virtual void setGamma(float gamma);

	// Method that calculate camera shift based hit cursor input
	virtual glm::vec3 updateCameraTarget(glm::vec2 cursor_position);

	virtual glm::vec3 getPosition();
	virtual glm::vec3 getDirection();
	virtual glm::vec3 getUpDirection();

	Camera(glm::vec2 window_size);
	virtual ~Camera();

	virtual bool isCameraReady();

	virtual glm::mat4x4 getProjectionMatrix();
	
	virtual glm::mat4x4 getViewMatrix();
	

	virtual void reshapeCallback(std::uint16_t width, std::uint16_t height);

	virtual void keyCallback(int key, int scancode, int action, int mods);

	virtual void cursorPosCallback(double xpos, double ypos);

	virtual void mouseButtonCallback(int button, int action, int mods);

	virtual bool init(std::vector<Shader> shaderPrograms);

	virtual bool frame(float delta_time, std::vector<Shader> shaderPrograms, int lightNumber);


};