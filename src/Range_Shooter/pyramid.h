#pragma once

#include "libs.h"


class Pyramid: public Object
{
protected:
	gl::GLuint sampler_handle;


	glm::vec3 model_position = glm::vec3(0.f, 0.f, 0.f);

	glm::mat4x4 model_matrix;


	float rotation_angle = 0.f;
	float rotation_speed = 2.f;
	glm::mat4x4 rotation_matrix;
	

public:
	Pyramid();
	virtual ~Pyramid();

	virtual glm::vec3 getPosition();
	virtual void setPosition(glm::vec3 new_position);

	virtual glm::mat4x4 getMatrix();
	virtual void setHit();

	
	virtual bool init();

	virtual bool frame(float delta_time) ;

	virtual bool draw();

	
};
