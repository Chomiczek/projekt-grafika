#pragma once

#include "../libs/OGLAppFramework/oglappframework.h"
#include "../libs/OGLAppFramework/utilities.h"
#include <map>

class Shader
{
private:
	// Shader ID and handle
	gl::GLuint shaderID;

	// Map of shader uniforms handle with string name 
	std::map<std::string, gl::GLuint> shaderUniforms;

	bool isReady = false;


public:
	Shader()
	{

	}

	// Sets new program for shader with VS and FS from given paths
	Shader(std::string vsPath, std::string fsPath)
	{
		setShader(vsPath, fsPath);
	}

	// Sets new program for shader with VS and FS from given paths
	void setShader(std::string vsPath, std::string fsPath)
	{
		{
			std::cout << "\t# " << typeid(*this).name() << ": Loading Shader:" << std::endl;
			std::cout << "\t\t " << "Vertex Shader:" << vsPath << std::endl;
			std::cout << "\t\t " << "Fragment Shader:" << fsPath << std::endl;
			if (auto create_program_result = OGLA::createProgram(vsPath, fsPath))
			{
				shaderID = create_program_result.value();
			}
			else
			{
				std::cerr << "\t\t " << "ERROR - can't create shader program" << std::endl;
				shaderID = -1;
			}
			isReady = true;

		}
	}

	// Adds uniform for shader with given name and handle. 
	// Does not generates uniform object
	gl::GLuint setUniform(std::string uniform_name, gl::GLuint handle)
	{

		shaderUniforms.insert(std::pair<std::string, gl::GLuint>(uniform_name, handle));
		return shaderUniforms.find(uniform_name)->second;
	}

	// Adds uniform for shader with given name and biding
	// Generates uniform object
	gl::GLuint setUniform(std::string uniform_name, const gl::GLuint binding_index, const gl::GLsizeiptr size, gl::GLenum use_mode = gl::GL_DYNAMIC_DRAW)
	{
		gl::GLuint ubo_handle = 0u;
		gl::glGenBuffers(1, &ubo_handle);
		gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, ubo_handle);

		gl::glBufferData(gl::GL_UNIFORM_BUFFER, size, nullptr, use_mode);
		// odbindowanie buffora zbindowanego jako UBO (zeby przypadkiem nie narobic sobie klopotow...)
		gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);
		gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, binding_index, ubo_handle);
		shaderUniforms.insert(std::pair<std::string, gl::GLuint>(uniform_name, ubo_handle));
		return shaderUniforms.find(uniform_name)->second;

	}

	// Returns handle to uniform with given name
	// If no uniform found, returns -1
	gl::GLuint getUniformHandle(std::string uniform_name)
	{
		std::map<std::string, gl::GLuint>::iterator it = shaderUniforms.find(uniform_name);
		if (it == shaderUniforms.end()) return -1;
		else return it->second;

	}
	
	// Updates uniform data of given name
	template<typename T>bool updateUniform(std::string uniform_name, T* data, const gl::GLsizeiptr size, bool ignoreNotFound=false)
	{
		
		if (shaderUniforms.count(uniform_name) < 1)
		{
			if(!ignoreNotFound) std::cout << "\t# " << typeid(*this).name() << "(ID " << shaderID << "): Uniform \"" << uniform_name << "\" not found . . . " << std::endl;
			return false;
		}
		use();
		gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, shaderUniforms[uniform_name]);
		// przygotownaie danych dla GPU
		// stworzenie bufora sierocego dla bufora zbindowanego jako UBO
		gl::glBufferData(gl::GL_UNIFORM_BUFFER, size, nullptr, gl::GL_DYNAMIC_DRAW);
		// mapowanie pamieci bufora zbindowanego jako UBO z mozliwoscia nadpisywania danych
		if (void* result = gl::glMapBuffer(gl::GL_UNIFORM_BUFFER, gl::GL_WRITE_ONLY))
		{
			// nadpisanie danych w buforze
			std::memcpy(result, data, size);
			// odmapowanie pamieci bufora zbindowanego jako UBO
			gl::glUnmapBuffer(gl::GL_UNIFORM_BUFFER);
		}
		// odbindowanie buffora zbindowanego jako UBO (zeby przypadkiem nie narobic sobie klopotow...)
		gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);
		return true;
	}
	
	// Sets current shader as used program
	bool use()
	{
		if (!isReady) return false;
		if (shaderID == -1) return false;
		gl::glUseProgram(shaderID);
		//std::cout << "#[" << "Using Shader " << shaderID << "]"<<std::endl;
		return true;
	}
	



};