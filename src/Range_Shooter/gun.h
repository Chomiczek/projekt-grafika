#pragma once

#include "libs.h"
#include "model.h"
#include "camera.h"
#include "laserbeam.h"

#define MAGAZINE_SIZE 12

class Weapon : public Model
{
protected:
	Camera* camera;
	Shader beamModelShader;
	Shader beamLightShader;

	// Default interval between beams
	float rateOfFire = 0.35f;
	float shootCooldown = 0.f;

	LaserBeam beams[MAGAZINE_SIZE];
	bool activeBeams[MAGAZINE_SIZE];
	
	void modelInit();
	void modelFrame(float delta_time);
	void modelDraw();

	void beamsInit();
	void beamsFrame(float delta_time);
	

	void activateLaserbeam();

public:
	Weapon();
	virtual ~Weapon();

	//Returns all laser lights that are currently active
	virtual std::vector<LaserBeam*> getLaserPtrs();

	virtual void setShader(Shader weaponShader, Shader beamModelShader, Shader beamLightShader);
	
	virtual void setCamera(Camera* cam);

	virtual void mouseButtonCallback(int button, int action, int mods);

	virtual bool init();
	
	virtual bool frame(float delta_time);

	virtual bool draw();



};