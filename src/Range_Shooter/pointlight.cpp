#include "pointlight.h"


PointLight::PointLight()
{
}

PointLight::~PointLight()
{
}

bool PointLight::setPointLight(glm::vec3 position, float lightRadius, glm::vec3 color)
{
	light = { position,lightRadius,color };
	return true;
}

glm::vec3 PointLight::getPosition()
{
	return light.position;
}

bool PointLight::changeLightPosition(glm::vec3 position)
{
	light.position = position;

	return true;
}

glm::vec3 PointLight::getLightColor()
{
	return light.color;
}


bool PointLight::draw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
	}

	shaderProgram.updateUniform<Point_Light>("PointLight", &light, sizeof(Point_Light));
	return true;
}

