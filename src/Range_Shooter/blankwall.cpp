#pragma once
#include "blankwall.h"


float HUD::MagazineState(int index)
{
	float multi =  BAR_SIZE/ MAGAZINE_SIZE;
	int bulletNum = gun->getLaserPtrs().size();
	//return 1.0f-  (1.0f+multi * bulletNum)/(BAR_SIZE - index) ;
	if (index >= BAR_SIZE - multi* bulletNum) return 0.0f;
	else return .5f;


}

HUD::HUD()
{
}

HUD::~HUD()
{
}

void HUD::setCamera(Camera* cam)
{
	camera = cam;
}

void HUD::setGun(Weapon* gun)
{
	this->gun = gun;
}


bool HUD::init()
{

	const gl::GLuint vertex_position_loction = 0u;

	std::vector<gl::GLfloat> vertices = {
	-1.f, -1.f, 0.0f,
	 1.f, -1.f, 0.0f,
	 1.0f,  1.f, 0.0f,

	 1.0f,  1.f, 0.0f,
	 -1.0f,  1.f, 0.0f,
	 -1.f, -1.f, 0.0f
	};
	
#pragma region VAO and VBO Setup
	
	gl::GLuint vbo_handle = generateVBO(vertices);

	gl::glGenVertexArrays(1, &objectVAO);
	gl::glBindVertexArray(objectVAO);
	gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo_handle);

	gl::glVertexAttribPointer(vertex_position_loction, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 3, nullptr);
	gl::glEnableVertexAttribArray(vertex_position_loction);
	gl::glVertexAttribDivisor(vertex_position_loction, 0);
	gl::glBindVertexArray(0u);
	gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
	
	if (vbo_handle)
	{

		gl::glDeleteBuffers(1, &vbo_handle);
		vbo_handle = 0u;
	}

#pragma endregion
	
	for(int i=0;i<BAR_SIZE;i++)
	{
		model_position.push_back(glm::vec3(-1.f, -.5f+i*.055f, -1.0f));
		magData[i] = glm::vec4(0.0f);
		model_matrix[i] = OGLA::translationMatrix(model_position[i]);
		model_matrix[i] *= OGLA::scaleMatrix(0.03 + 0.05f - i * 0.005, 0.02f, 1.0f);

	}
	


	shaderProgram.updateUniform<glm::mat4x4[BAR_SIZE]>("HUDModel", &model_matrix, BAR_SIZE * sizeof(glm::mat4x4));
	return true;



}

bool HUD::frame(float delta_time)
{
	
	for (int i = 0; i < BAR_SIZE; i++)
	{
		magData[i] = glm::vec4(MagazineState(i),0.0f,0.0f,0.0f);
	}
	

	return true;
}

bool HUD::draw()
{

	gl::glDepthRange(0.0, 0.01);
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
	}
	shaderProgram.updateUniform<glm::vec4[BAR_SIZE]>("Mag", &magData, BAR_SIZE * sizeof(glm::vec4));
	
	gl::glBindVertexArray(objectVAO);
	
	gl::glDrawArraysInstanced(gl::GL_TRIANGLES, 0, 6,BAR_SIZE);
		
	
	gl::glBindVertexArray(0u);
	gl::glDepthRange(0.01, 1);
	return false;
}

