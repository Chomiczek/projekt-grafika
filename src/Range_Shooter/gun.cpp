#include "gun.h"

void Weapon::modelInit()
{
	model_position = glm::vec3(0.0f, -0.7f, -3.f);

#pragma region Locations and Indices
	// ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_position_loction = 0u;
	// ustawienie informacji o lokalizacji atrybutu uv w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_tex_uv_loction = 1u;
	// ustawienie informacji o lokalizacji atrybutu wektora normalnego w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_normal_loction = 2u;

#pragma endregion
	prepareModel(vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);
	
}

void Weapon::modelFrame(float delta_time)
{
	
	model_matrix = glm::inverse(camera->getViewMatrix());
	model_matrix *= OGLA::translationMatrix(model_position);
	model_matrix *= OGLA::rotationMatrix(glm::pi<float>(), glm::vec3(.0f, 1.0f, .0f));
	
}

void Weapon::modelDraw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
		return;
	}

	shaderProgram.updateUniform<glm::mat4x4>("Model", &model_matrix, sizeof(glm::mat4x4));

	updateMaterialUniform();
	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);
	drawOnScreen();
	gl::glBindTexture(gl::GL_TEXTURE_2D, 0u);
}

void Weapon::beamsInit()
{

	for (int i = 0; i < MAGAZINE_SIZE; i++)
	{
		activeBeams[i] = false;
		beams[i].setShader(beamModelShader, beamLightShader);
		beams[i].init();
	}

}

void Weapon::beamsFrame(float delta_time)
{


	for (int i = 0; i < MAGAZINE_SIZE; i++)

	{
		if (activeBeams[i])
		{
			beams[i].frame(delta_time);
			if (beams[i].getTimeLeft() <= 0)
			{
				activeBeams[i] = false;
				beams[i].setBeam(glm::vec3(0.0f), glm::vec3(0.0f), 0.f, 0.f, glm::vec3(0.f,0.f,0.f));
			}
		}
	}
}

void Weapon::activateLaserbeam()
{
	int index = 0;
	for (index = 0; index < MAGAZINE_SIZE; index++)
	{
		if (!activeBeams[index]) break;
	}
	if (index == MAGAZINE_SIZE)
	{
		std::cout << "\t# " << typeid(*this).name() << " overheat ... Wait for it to cool down ..." << std::endl;
		return;
	}
	glm::vec3 chamber = glm::vec3(0.0f, -0.6f,0.0f);
	glm::vec3 cam_normal = camera->getDirection();
	cam_normal = glm::normalize(cam_normal);
	beams[index].setBeam(camera->getPosition() +chamber , camera->getDirection(),150.f,1.f);
	beams[index].matrix *= OGLA::rotationMatrix(glm::half_pi<float>() - glm::acos(glm::dot(camera->getPosition(), cam_normal)), camera->getUpDirection());
	activeBeams[index] = true;
}

Weapon::Weapon()
{
}

Weapon::~Weapon()
{
}

std::vector<LaserBeam*> Weapon::getLaserPtrs()
{
	std::vector<LaserBeam*> lights;
	for (int i = 0; i < MAGAZINE_SIZE; i++)
		if (activeBeams[i]) 
			lights.push_back(&beams[i]);
	return lights;

}

void Weapon::setShader(Shader weaponShader, Shader beamModelShader, Shader beamLightShader)
{
	shaderProgram = weaponShader;
	this->beamModelShader = beamModelShader;
	this->beamLightShader = beamLightShader;
}

void Weapon::setCamera(Camera* cam)
{
	camera = cam;
}


void Weapon::mouseButtonCallback(int button, int action, int mods)
{
	if (button == 0)
	{
		if (shootCooldown <= 0)
		{
			shootCooldown = rateOfFire;
			activateLaserbeam();

		}
	}
}


bool Weapon::init()
{
	
	

	modelInit();
	beamsInit();


	return true;
}

bool Weapon::frame(float delta_time)
{
	shootCooldown -= delta_time;
	if (shootCooldown <0) shootCooldown = 0;
	modelFrame(delta_time);
	beamsFrame(delta_time);
	
	return true;
}

bool Weapon::draw()
{
	gl::glDepthRange(0.01, 0.02);
	modelDraw();
	for (int index = 0; index < MAGAZINE_SIZE; index++)
	{
		if (activeBeams[index]) beams[index].draw();
	}
	gl::glDepthRange(0.02, 1);
	return true;
}




