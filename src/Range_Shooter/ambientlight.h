#pragma once
#include "libs.h"
#include "light.h"


class AmbientLight : public Light
{
protected:

	// Contains Ambient Light Color
	glm::vec3 ambient_light;
	Point_Light light;

public:
	AmbientLight();
	virtual ~AmbientLight();

	// Sets color of Ambient Light
	virtual bool setAmbientLight(glm::vec3 color);

	virtual bool draw();

};