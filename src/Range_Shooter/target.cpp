#include "target.h"

void Target::setShape()
{
	std::vector<gl::GLfloat> vertices = {
	-1.f, -1.f, 0.0f, 
	 1.f, -1.f, 0.0f, 
	 1.0f,  1.f, 0.0f, 

	 1.0f,  1.f, 0.0f, 
	 -1.0f,  1.f, 0.0f, 
	 -1.f, -1.f, 0.0f,
	};
	


	shapeCollider.vertices.clear();
	shapeCollider.vertices = vertices;
	shapeCollider.indices = { 0,1, 2, 3,4,5 };

}

Target::Target()
{
}

Target::~Target()
{
}



void Target::setHit()
{
	hit = true;
	cooldown = 1.f;
}

bool Target::getIsHit()
{
	return hit;
}

void Target::setUpMatrix(glm::mat4x4 mat)
{
	model_matrix = OGLA::translationMatrix(model_position);
	model_matrix *= OGLA::rotationMatrix(glm::pi<float>(), glm::vec3(0.0f, 0.0f, 1.0f));
	model_matrix *= OGLA::scaleMatrix(1.f, 1.f, 1.f);
	model_matrix *= mat;
}


bool Target::init()
{
	
	orientation = glm::vec3(1.f, 0.f, 0.f);
	//std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;

#pragma region Locations and Indices
	// ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_position_loction = 0u;
	// ustawienie informacji o lokalizacji atrybutu uv w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_tex_uv_loction = 1u;
	// ustawienie informacji o lokalizacji atrybutu wektora normalnego w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_normal_loction = 2u;
	

	

	prepareModel(vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);
	
	setMaterial(glm::vec3(1.f, 1.f, 1.f), 1.f, 10000.f);
	setShape();
	
	return true;
}

bool Target::frame(float delta_time)
{
	if (hit) cooldown -= delta_time;
	if (cooldown < 0) hit = false;
	return true;
}

bool Target::draw()
{
	if (hit) return true;
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
		return false;
	}

	shaderProgram.updateUniform<glm::mat4x4>("Model", &model_matrix, sizeof(glm::mat4x4));

	updateMaterialUniform();
	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);
	drawOnScreen();
	gl::glBindTexture(gl::GL_TEXTURE_2D, 0u);
	return true;
}



