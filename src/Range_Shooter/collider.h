#pragma once
#include "libs.h"
#include "object.h"
#include "gun.h"


static class Collider
{

protected:

	// Returns plane determinated by 3 points
	static glm::vec4 getPlane(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
	{
		glm::vec3 plane = glm::normalize(glm::cross(p3 - p1, p2 - p1));
		float alpha = -1 * glm::dot(plane, p1);

		return glm::vec4(plane, alpha);

	}
	
	static bool checkRayHit(glm::vec3 p1, glm::vec3 p2, glm::vec3 cross_point, glm::vec4 plane)
	{
		return glm::dot(glm::cross(p2 - p1, cross_point - p1), glm::vec3(plane))<0;
	}

	// Returns point of cross beetwen plane and vector (orgin, direction)
	static float getCrossPoint(glm::vec4 plane, glm::vec3 orgin, glm::vec3 direction)
	{
		return -((glm::dot(glm::vec3(plane), orgin)+plane.w)
			/ glm::dot(glm::vec3(plane), direction));

	}

	static glm::vec3 getPoint(Object::Shape* collider,int index)
	{
		return glm::vec3(collider->vertices[3 * index], collider->vertices[3 * index + 1], collider->vertices[3 * index + 2]);
	}
public:
	// Returns new direction for given rey and plane
	static glm::vec3 getRayReflection(glm::vec4 plane, glm::vec3 orgin, glm::vec3 direction)
	{
		return direction - 2 * glm::dot(direction, glm::vec3(plane)) * glm::vec3(plane);
	}

	// Reycasting hit given shape (shape, model_matrix) base hit given rey (orgin, direction)
	// Returns true if reycast was succesfull and shape is in given proximity
	static bool reycastImpact(Object::Shape* collider, glm::mat4x4 model_matrix, glm::vec3 orgin, glm::vec3 direction,  float proximity = 1.0f)
	{
		if (model_matrix == glm::mat4x4()) return false;
		if (collider->vertices.size() == 0 || collider->indices.size() == 0) return false;
		//std::cout << "Size: " << collider->indices.size() << std::endl;
		glm::vec3 cross_p;
		glm::vec3 p1, p2, p3;
		glm::vec3 pp1, pp2, pp3;
		float t;
		int i;
		glm::vec4 plane;
		//std::cout << "Size: " << coll.vertices.size() << std::endl;
		for (i = 0; i < collider->indices.size(); i+=3)
		{
			
			p1 = model_matrix * glm::vec4(getPoint(collider,collider->indices[i]), 1.0f);
			p2 = model_matrix * glm::vec4(getPoint(collider, collider->indices[i+1]), 1.0f);
			p3 = model_matrix * glm::vec4(getPoint(collider, collider->indices[i+2]), 1.0f);
			plane = getPlane(p1, p2, p3);
			if (glm::dot(glm::vec3(plane), direction) != 0)
			{

				t = getCrossPoint(plane, orgin, direction);

				if (t < 0) continue;

				cross_p = orgin + t * direction;

				if (checkRayHit(p1, p2, cross_p, plane)&&
					checkRayHit(p2, p3, cross_p, plane)&&
						checkRayHit(p3, p1, cross_p, plane))
						{
							
							
					if (glm::abs(t) <= proximity)
					{
						//std::cout << "P = [" << cross_p.x << ", " << cross_p.y << ", " << cross_p.z << "] T-" << t << std::endl;
						return true;
					}
					else return false;
				}

			}
		}
		return false;
	}

	// Reycasting hit given shape (shape, model_matrix) base hit given rey (orgin, direction)
	// Returns true if reycast was succesfull and shape is in given proximity
	static bool reycastImpact(Object::Shape* collider, glm::mat4x4 model_matrix, glm::vec3 orgin, glm::vec3 direction, glm::vec3 &pointOfImpact, float &timeToImpact, float proximity = 1.0f)
	{
		
		//std::cout << "Size: " << collider->indices.size() << std::endl;
		glm::vec3 cross_p;
		glm::vec3 p1, p2, p3;
		glm::vec3 pp1, pp2, pp3;
		float t;
		int i;
		glm::vec4 plane;
		//std::cout << "Size: " << coll.vertices.size() << std::endl;
		for (i = 0; i < collider->indices.size(); i += 3)
		{

			p1 = model_matrix * glm::vec4(getPoint(collider, collider->indices[i]), 1.0f);
			p2 = model_matrix * glm::vec4(getPoint(collider, collider->indices[i + 1]), 1.0f);
			p3 = model_matrix * glm::vec4(getPoint(collider, collider->indices[i + 2]), 1.0f);
			plane = getPlane(p1, p2, p3);
			if (glm::dot(glm::vec3(plane), direction) != 0)
			{

				t = getCrossPoint(plane, orgin, direction);

				if (t < 0) continue;

				cross_p = orgin + t * direction;

				if (checkRayHit(p1, p2, cross_p, plane) &&
					checkRayHit(p2, p3, cross_p, plane) &&
					checkRayHit(p3, p1, cross_p, plane))
				{


					if (glm::abs(t) <= proximity)
					{
						//std::cout << "P = [" << cross_p.x << ", " << cross_p.y << ", " << cross_p.z << "] T-" << t << std::endl;
						pointOfImpact = cross_p;
						timeToImpact = t;
						return true;
					}
					else return false;
				}

			}
		}
		return false;
	}

	// Reycasting hit all object hit scene
	static bool reycastOnAll(std::vector<Object*> objects, glm::vec3 orgin, glm::vec3 &direction, float proximity = 10.0f)
	{
		bool result = false;
		glm::vec3 pointOfImpact = glm::vec3();
		float timeToImpact=0;
		for each (Object * obj in objects)
		{
			if (typeid(*obj) == typeid(Weapon)) continue;
			if (obj->getIsHit()) continue;
			result = reycastImpact(obj->getShapePtr(), obj->getMatrix(), orgin, direction, pointOfImpact, timeToImpact, proximity);
			if (result)
			{
				//std::cout << "Hit in << T: " << timeToImpact << "  with " << typeid(*obj).name() << " At [" << pointOfImpact.x << ", " << pointOfImpact.y << ", " << pointOfImpact.z << "]" << std::endl;
				obj->setHit();
				return true;
			}
		}
		return false;



	}

	
	// Simple Collider based hit proximity and position
	static bool proximityCollider(glm::vec3 beam, glm::vec3 object, glm::vec3 objectSize = glm::vec3(0.5, 0.5, 0.2))
	{
		glm::vec3 result = glm::abs( object - beam);

		if (result.x <= objectSize.x && result.y <= objectSize.y && result.z <= objectSize.z) return true;
		return false;
	}
};



// TO DELETE WHEN NOT NEEDED
class ColliderVisualizer: public Object
{

	glm::mat4x4 model_matrix;
	glm::vec3 color = glm::vec3(1.0f, 1.0f, 1.0f);


public:

	virtual void setMatrix(glm::mat4x4 mat)
	{
		model_matrix = mat;
	}

	virtual void setCollider(std::vector<glm::vec3>  object)
	{
		shapeCollider.vertices.clear();
		for each (glm::vec3 var in object)
		{
			shapeCollider.vertices.push_back(var.x);
			shapeCollider.vertices.push_back(var.y);
			shapeCollider.vertices.push_back(var.z);
		}
		shapeCollider.indices.clear();

		shapeCollider.indices.push_back(0);
		shapeCollider.indices.push_back(1);
		shapeCollider.indices.push_back(2);
		shapeCollider.indices.push_back(2);
		shapeCollider.indices.push_back(3);
		shapeCollider.indices.push_back(0);
		std::cout << "\t\t";
		
		std::cout << std::endl;
	}

	void setCollider(Object::Shape object)
	{
		shapeCollider = { object.vertices,object.indices };
		//std::cout << "Size: " << collider.indices.size() << std::endl;
	}

	// metoda wywolywana na poczatku (przy starcie aplikacji, po inicjalizacji OGL)
	virtual bool init()
	{
		

		generateObject(shapeCollider.vertices, shapeCollider.indices,0u,1u,2u);
		indicesNumber = shapeCollider.indices.size();

		return true;
	}
	// metoda wywolywana co klatke
	virtual bool frame(float delta_time)
	{


		return true;
	}

	virtual bool draw()
	{
		if (!shaderProgram.use())
		{
			std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
		}

		shaderProgram.updateUniform<glm::mat4x4>("Model", &model_matrix, sizeof(glm::mat4x4));
		shaderProgram.updateUniform<glm::vec3>("LaserLight", &color, sizeof(glm::vec3));

		drawOnScreen();


		return true;
	}

	virtual void Visualise(glm::mat4x4 mat, float delta_time)
	{
		//std::cout.setstate(std::ios_base::failbit);
		setMatrix(mat);

		init();
		frame(delta_time);
		draw();



		//std::cout.clear();
	}

};