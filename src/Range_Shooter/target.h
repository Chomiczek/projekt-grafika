#pragma once

#include "libs.h"
#include "model.h"


class Target : public Model
{

	glm::vec3 orientation;
	bool hit = false;
	float cooldown = 10.f;
	
	void setShape();

public:
	Target();
	virtual ~Target();

	virtual glm::mat4x4 getMatrix()
	{
		return model_matrix;
	}
	
	virtual void setHit();
	virtual bool getIsHit();
	virtual void setUpMatrix(glm::mat4x4 mat);


	virtual bool init();
	// metoda wywolywana co klatke
	virtual bool frame(float delta_time);

	virtual bool draw();
};