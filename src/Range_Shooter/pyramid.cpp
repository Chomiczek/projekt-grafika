#pragma once
#include "pyramid.h"

using namespace OGLA;

Pyramid::Pyramid() : Object::Object(), sampler_handle(0u)
{
}

Pyramid::~Pyramid()
{
}

glm::vec3 Pyramid::getPosition()
{
	return model_position;
}

void Pyramid::setPosition(glm::vec3 new_position)
{
	model_position = new_position;
	model_matrix = OGLA::translationMatrix(model_position);
}

glm::mat4x4 Pyramid::getMatrix()
{
	return model_matrix;
}

void Pyramid::setHit()
{
	if (rotation_speed <= 0.f) rotation_speed = 0.f;
	rotation_speed += 5.f;
}




bool Pyramid::init()
{

	//std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;


	//wczytywanie tekstury
	loadTexture("../../../data/Modern_diffiuse.dds");



#pragma region Locations and Indices
	// ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_position_loction = 0u;
	// ustawienie informacji o lokalizacji atrybutu uv w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_tex_uv_loction = 1u;
	// ustawienie informacji o lokalizacji atrybutu wektora normalnego w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_normal_loction = 2u;
	

	
#pragma endregion

#pragma region Verticies Definition
	// stworzenie tablicy z danymi o wierzcholkach 3x (x, y, z) i wsp�rz�dnych uv 2x (u, v)
	std::vector<gl::GLfloat> vertices = {
		-1.f, -1.f, 1.f,		0.f, 1.f,		0.f, 0.447f, 0.894f,
		1.f, -1.f, 1.f,			1.f, 1.f,		0.f, 0.447f, 0.894f,
		0.f, 1.f, 0.f,			0.5f, 0.f,		0.f, 0.447f, 0.894f,

		1.f, -1.f, 1.f,			0.f, 1.f,		0.894f, 0.447f, 0.f,
		1.f, -1.f, -1.f,		1.f, 1.f,		0.894f, 0.447f, 0.f,
		0.f, 1.f, 0.f,			0.5f, 0.f,		0.894f, 0.447f, 0.f,

		1.f, -1.f, -1.f,		0.f, 1.f,		0.f, 0.447f, -0.894f,
		-1.f, -1.f, -1.f,		1.f, 1.f,		0.f, 0.447f, -0.894f,
		0.f, 1.f, 0.f,			0.5f, 0.f,		0.f, 0.447f, -0.894f,

		-1.f, -1.f, -1.f,		0.f, 1.f,		-0.894f, 0.447f, 0.f,
		-1.f, -1.f, 1.f,		1.f, 1.f,		-0.894f, 0.447f, 0.f,
		0.f, 1.f, 0.f,			0.5f, 0.f,		-0.894f, 0.447f, 0.f,

		-1.f, -1.f, 1.f,		0.f, 1.f,		0.f, -1.f, 0.f,
		-1.f, -1.f, -1.f,		0.f, 0.f,		0.f, -1.f, 0.f,
		1.f, -1.f, 1.f,			1.f, 1.f,		0.f, -1.f, 0.f,

		1.f, -1.f, -1.f,		1.f, 0.f,		0.f, -1.f, 0.f,
		1.f, -1.f, 1.f,			1.f, 1.f,		0.f, -1.f, 0.f,
		-1.f, -1.f, -1.f,		0.f, 0.f,		0.f, -1.f, 0.f
	};

	// stworzenie tablicy z danymi o indeksach
	std::vector<gl::GLushort> indices = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
	indicesNumber = indices.size();
#pragma endregion
	setShape(vertices, indices);



#pragma region VAO and VBO Setup

	generateObject(vertices, indices, vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);
	gl::glBindVertexArray(objectVAO);
#pragma endregion

#pragma region Sampler Generation and Parametri
	// Tworzenie SO
	gl::glGenSamplers(1, &sampler_handle);
	// Ustawienie parametr�w samplowania
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_R, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
#pragma endregion

	model_matrix = OGLA::translationMatrix(model_position);


#pragma region UBO Material

	setMaterial(glm::vec3(1.f, 1.f, 1.f), 1.f, 10000.f);

#pragma endregion

	rotation_matrix = model_matrix * OGLA::rotationMatrix(-rotation_angle, glm::vec3(0.f, 1.f, 0.f));
#pragma region Sampler and Texture Binding
	// przyporzadkowanie sampler-a do pierwszego slotu tekstur
	gl::glBindSampler(0, sampler_handle);
	// uaktywnienie pierwszego slotu tekstur
	gl::glActiveTexture(gl::GL_TEXTURE1);
	// zbindowanie tekstury do aktywnego slotu
	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);
#pragma endregion
	gl::glBindVertexArray(0u);
	return true;



}

bool Pyramid::frame(float delta_time)
{
	
	rotation_speed -= delta_time;
	if (rotation_speed <= 0.f) rotation_speed = 0.f;
	// update rotacji piramid
	rotation_angle = std::fmodf(rotation_angle + delta_time * rotation_speed, 360.f);
	//std::cout << "Rotation: " << rotation_angle << " with speed: " << rotation_speed << std::endl;
	rotation_matrix = model_matrix*OGLA::rotationMatrix(rotation_angle, glm::vec3(0.f, 1.f, 0.f));
	

	return true;
}

bool Pyramid::draw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
	}

	shaderProgram.updateUniform<glm::mat4x4>("Model", &rotation_matrix, sizeof(glm::mat4x4));


	updateMaterialUniform();

	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);

	drawOnScreen();

	gl::glBindTexture(gl::GL_TEXTURE_2D, 0u);


	return false;
}

