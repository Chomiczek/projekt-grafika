#pragma once
#include "floor.h"

using namespace OGLA;

Floor::Floor() : sampler_handle(0u)
{
}

Floor::~Floor()
{
}

glm::vec3 Floor::getPosition()
{
	return model_position;
}

void Floor::setPosition(glm::vec3 new_position)
{
	model_position = new_position;
	model_matrix = OGLA::translationMatrix(model_position);
}

glm::mat4x4 Floor::getMatrix()
{
	return model_matrix;
}



bool Floor::init()
{

	//std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;


	//wczytywanie tekstury
	loadTexture("../../../data/Modern_diffiuse.dds");



#pragma region Locations and Indices
	// ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_position_loction = 0u;
	// ustawienie informacji o lokalizacji atrybutu uv w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_tex_uv_loction = 1u;
	// ustawienie informacji o lokalizacji atrybutu wektora normalnego w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_normal_loction = 2u;
	

	
#pragma endregion

#pragma region Verticies Definition
	// stworzenie tablicy z danymi o wierzcholkach 3x (x, y, z) i wsp�rz�dnych uv 2x (u, v)
	
	shapeCollider = create3DCylinder(0.01f, 10.f, 8,1000.f);

	indicesNumber = shapeCollider.indices.size();
#pragma endregion
	

#pragma region VAO and VBO Setup

	generateObject(shapeCollider.vertices, shapeCollider.indices, vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);
	gl::glBindVertexArray(objectVAO);
#pragma endregion

#pragma region Sampler Generation and Parametri
	// Tworzenie SO
	gl::glGenSamplers(1, &sampler_handle);
	// Ustawienie parametr�w samplowania
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_R, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
#pragma endregion
	
#pragma region Class Field Initialization
	// inicjalizacja p�l klasy
	model_matrix = OGLA::translationMatrix(model_position);
	model_matrix *= OGLA::rotationMatrix(glm::half_pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f));
	model_matrix *= OGLA::scaleMatrix(200.0f, 200.f, 200.f);
#pragma endregion

#pragma region UBO Material

	setMaterial(glm::vec3(1.f, 1.f, 1.f), 1.f, 10000.f);

#pragma endregion

	
#pragma region Sampler and Texture Binding
	// przyporzadkowanie sampler-a do pierwszego slotu tekstur
	gl::glBindSampler(0, sampler_handle);
	// uaktywnienie pierwszego slotu tekstur
	gl::glActiveTexture(gl::GL_TEXTURE1);
	// zbindowanie tekstury do aktywnego slotu
	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);
#pragma endregion
	gl::glBindVertexArray(0u);
	return true;



}


bool Floor::draw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
	}

	shaderProgram.updateUniform<glm::mat4x4>("Model", &model_matrix, sizeof(glm::mat4x4));


	updateMaterialUniform();

	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);

	drawOnScreen();

	gl::glBindTexture(gl::GL_TEXTURE_2D, 0u);


	return false;
}
