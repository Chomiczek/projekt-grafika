#include "libs.h"

class Cubemap : public Object
{
private:
	gl::GLuint sampler_handle;
	gl::GLuint cubemapID;
	

public:
	Cubemap();
	Cubemap(Shader shader);
	
	virtual ~Cubemap();

	virtual void setCubeTexture(std::string cube_texture_path);

	virtual bool init();
	
	virtual bool draw();



};