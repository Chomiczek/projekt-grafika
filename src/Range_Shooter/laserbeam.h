#pragma once
#include "libs.h"
#include "pointlight.h"

class LaserBeam: public Object
{
protected:

	PointLight light;
	glm::vec3 beamPosition;
	
	glm::vec3 beamDirection;
	float beamSpeed;
	float timeToDesintegrate;



public:
	glm::mat4x4 matrix;

	LaserBeam();
	~LaserBeam();

	virtual void setHit(bool isHit);

	virtual glm::vec3 getPosition();

	virtual glm::vec3 getDirection();

	virtual float getTimeLeft();

	// Returns pointer to beam light
	virtual PointLight* getLightPtr();

	// Set shader program for beam and for beam light
	virtual void setShader(Shader beamShader, Shader lightShader);

	virtual void setBeam(glm::vec3 position, glm::vec3 direction, float speed = 100.f, float time = .5f,glm::vec3 color = glm::vec3(0.f, 2.f, 0.f));


	virtual bool init();

	virtual bool frame(float delta_time);

	virtual bool draw();

};
