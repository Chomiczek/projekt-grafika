#pragma once
#include "libs.h"
#include "light.h"


class PointLight : public Light
{
protected:

	Point_Light light;

public:
	PointLight();
	virtual ~PointLight();
	
	// Sets point light
	virtual bool setPointLight(glm::vec3 position, float lightRadius, glm::vec3 color);


	virtual glm::vec3 getPosition();
	virtual bool changeLightPosition(glm::vec3 position);

	virtual glm::vec3 getLightColor();

	
	
	virtual bool draw();

};