#include "laserbeam.h"

LaserBeam::LaserBeam()
{
}

LaserBeam::~LaserBeam()
{
}

void LaserBeam::setHit(bool isHit)
{
	if (isHit) timeToDesintegrate = 10.f;
}

glm::vec3 LaserBeam::getPosition()
{
	return beamPosition;
}

glm::vec3 LaserBeam::getDirection()
{
	return beamDirection;
}

float LaserBeam::getTimeLeft()
{
	return timeToDesintegrate;
}

PointLight* LaserBeam::getLightPtr()
{
	return &light;
}

void LaserBeam::setShader(Shader beamShader, Shader lightShader)
{
	this->shaderProgram = beamShader;
	light.setShader(lightShader);
}

void LaserBeam::setBeam(glm::vec3 position, glm::vec3 direction, float speed, float time, glm::vec3 color)
{
	light.setPointLight(beamPosition, 30.f, color);
	beamPosition = position;
	light.changeLightPosition(beamPosition);
	beamDirection = glm::normalize(direction);
	beamSpeed = speed;
	timeToDesintegrate = time;
	matrix = OGLA::translationMatrix(position);
	
}


bool LaserBeam::init()
{
	//std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;
	light.setPointLight(beamPosition, 30.f, glm::vec3(0.f, 2.f, 0.f));
	
#pragma region Locations and Indices
	// ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_position_loction = 0u;
	// ustawienie informacji o lokalizacji atrybutu uv w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_tex_uv_loction = 1u;
	// ustawienie informacji o lokalizacji atrybutu wektora normalnego w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_normal_loction = 2u;
#pragma endregion

	Shape cylinder = create3DCylinder(.05f, 0.05f, 360);
	indicesNumber = cylinder.indices.size();
	generateObject(cylinder.vertices, cylinder.indices, vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);
	return true;
}

bool LaserBeam::frame(float delta_time)
{
	
	timeToDesintegrate -= delta_time;
	matrix *= OGLA::translationMatrix(beamDirection * beamSpeed * delta_time);
	light.changeLightPosition(light.getPosition() + beamDirection * beamSpeed * delta_time);
	beamPosition = light.getPosition();
	
	return true;
}

bool LaserBeam::draw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
	}
	glm::vec3 color = light.getLightColor();
	shaderProgram.updateUniform<glm::mat4x4>("Model", &matrix, sizeof(glm::mat4x4));
	shaderProgram.updateUniform<glm::vec3>("LaserLight", &color, sizeof(glm::vec3));

	drawOnScreen();
	return true;
}

