#include "model.h"


Model::Model()
{
}

Model::~Model()
{
}

std::vector<glm::vec3> Model::getColloder()
{
	return std::vector<glm::vec3>();
}

glm::vec3  Model::getPosition()
{
	return model_position;
}

void Model::setPosition(glm::vec3 pos)
{
	
	model_matrix *= OGLA::translationMatrix(pos);
	model_position = pos;
}

void Model::setTexture(std::string texture_path)
{
	this->texture_path = texture_path;
}

void Model::setModelPath(std::string model_path)
{
	model_path_name = model_path;
	this->model_path = model_path.substr(0, model_path.find_last_of("/") + 1);
}

void Model::LoadObj(std::string model_path, std::string model_name)
{
	std::string warn;
	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, model_name.c_str(), model_path.c_str(), true);

	if (!warn.empty()) {
		std::cout << "WARN: " << warn << std::endl;
	}

	if (!err.empty()) {
		std::cerr << "ERR: " << err << std::endl;
	}

	if (!ret) {
		printf("Failed to load/parse .obj.\n");
	}

	
}

void Model::generateModel(gl::GLuint vertex_position_loction, gl::GLuint vertex_tex_uv_loction, gl::GLuint vertex_normal_loction)
{
	std::vector<gl::GLfloat> vertices;
	std::vector<gl::GLushort> indices;
	int n = 0;
	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		size_t index_offset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			int fv = shapes[s].mesh.num_face_vertices[f];

			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {

				// access to vertex
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 0]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 1]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 2]);

				vertices.push_back(attrib.texcoords[2* idx.texcoord_index +0]);
				vertices.push_back(attrib.texcoords[2*idx.texcoord_index + 1]);
				

				vertices.push_back(attrib.normals[3 * idx.normal_index + 0]);
				vertices.push_back(attrib.normals[3 * idx.normal_index + 1]);
				vertices.push_back(attrib.normals[3 * idx.normal_index + 2]);
				
				indices.push_back(n);
				n++;

				// Optional: vertex colors
				// tinyobj::real_t red = attrib.colors[3*idx.vertex_index+0];
				// tinyobj::real_t green = attrib.colors[3*idx.vertex_index+1];
				// tinyobj::real_t blue = attrib.colors[3*idx.vertex_index+2];
			}
			index_offset += fv;

			// per-face material
			//shapes[s].mesh.material_ids[f];
		}
	}
	indicesNumber = indices.size();
	generateObject(vertices, indices, vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);
	setShape(vertices, indices);
	//std::cout << "\t\t Vertice Size: "<< vertices.size();
	//std::cout << "  (Model Indictes Size: " << indices.size() << ")" << std::endl;


	setMaterial(glm::vec3(1.f, 1.f, 1.f), 1.f, 10000.f);
}

void Model::prepareModel(gl::GLuint vertex_position_loction, gl::GLuint vertex_tex_uv_loction, gl::GLuint vertex_normal_loction)
{
	LoadObj(model_path, model_path_name);
	generateModel(vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);

	gl::glGenTextures(0, &objectTexture);
	//gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);


	loadTexture(texture_path);
#pragma region Sampler Generation and Parametri
	// Tworzenie SO
	gl::glGenSamplers(1, &sampler_handle);
	// Ustawienie parametrów samplowania
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_WRAP_R, gl::GL_REPEAT);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR_MIPMAP_LINEAR);
	gl::glSamplerParameteri(sampler_handle, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
#pragma endregion

#pragma region Sampler and Texture Binding
	// przyporzadkowanie sampler-a do pierwszego slotu tekstur
	gl::glBindSampler(0, sampler_handle);
	// uaktywnienie pierwszego slotu tekstur
	gl::glActiveTexture(gl::GL_TEXTURE0);
	// zbindowanie tekstury do aktywnego slotu
	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);
}



bool Model::init()
{


	//std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;

#pragma region Locations and Indices
	// ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_position_loction = 0u;
	// ustawienie informacji o lokalizacji atrybutu uv w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_tex_uv_loction = 1u;
	// ustawienie informacji o lokalizacji atrybutu wektora normalnego w vs (musi sie zgadzac z tym co mamy w VS!!!)
	const gl::GLuint vertex_normal_loction = 2u;
	
#pragma endregion
	prepareModel(vertex_position_loction, vertex_tex_uv_loction, vertex_normal_loction);

	return true;
}


bool Model::draw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
		return false;
	}

	shaderProgram.updateUniform<glm::mat4x4>("Model", &model_matrix, sizeof(glm::mat4x4));

	updateMaterialUniform();
	gl::glBindTexture(gl::GL_TEXTURE_2D, objectTexture);
	drawOnScreen();
	gl::glBindTexture(gl::GL_TEXTURE_2D, 0u);
	return true;
}




