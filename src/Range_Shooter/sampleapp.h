#pragma once

#include "libs.h"




#include "camera.h"

#include "pyramid.h"
#include "floor.h"

#include "model.h"
#include "target.h"
#include "gun.h"
#include "collider.h"
#include "blankwall.h"
#include "cubemap.h"


#pragma region Light Include
#include "light.h"
#include "pointlight.h"
#include "ambientlight.h"
#include "laserbeam.h"
#pragma endregion

#define TARGET_NUM 6
#define LIGHT_NUM 4

class BaseApp : public OGLA::OGLApplication
{
    std::vector<Object*> objectsOnScene;
    std::vector<Shader> shaderPrograms;
    std::vector<Light*> lightOnScene;

    

    Camera camera;
    Cubemap skybox;

    Weapon gun;

    HUD hud;
    Floor floor;
    Pyramid pyramid;
    AmbientLight ambietLight;
    PointLight lightPoint1;
    PointLight lightPoint2;
    PointLight lightPoint3;

    PointLight lightPoint[LIGHT_NUM];
    Target targets[TARGET_NUM];
    
    void loadShaders();
    
    void addStaticLights();
    void setTargets();
    void setLights();

public:
    BaseApp();
    virtual ~BaseApp() override;


    void addObjectToScene(Object* obj);

    void addLightToScene(Light* obj);

    void setUniforms(int shaderIndex, std::vector<UniformData> uniformBindings);

    // metoda wywo�ywana podczas u�ycia scrollu myszki
    virtual void mouseScrollCallback(double xoffset, double yoffset) override;
    // metoda wywolywana podczas zmiany rozmiaru okna
    virtual void reshapeCallback(std::uint16_t width, std::uint16_t height) override;
    // metoda wywolywana podczas wcisniecia przycisku
    virtual void keyCallback(int key, int scancode, int action, int mods) override;
    // metoda wywolywana podczas zmiany pozycji kursora myszy
    virtual void cursorPosCallback(double xpos, double ypos) override;
    // metoda wywolywana podczas wcisniecia przycisku myszy
    virtual void mouseButtonCallback(int button, int action, int mods) override;
    // metoda wywolywana na poczatku (przy starcie aplikacji, po inicjalizacji OGL)
    virtual bool init(void) override;
    // metoda wywolywana co klatke
    virtual bool frame(float delta_time) override;
    // metoda wywolywana przy zamknieciu aplikacji
    virtual void release() override;
};
