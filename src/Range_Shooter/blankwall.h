#pragma once

#include "libs.h"
#include "camera.h"
#include "gun.h"


#define BAR_SIZE 12

class HUD : public Object
{
protected:
	gl::GLuint sampler_handle;
	Camera* camera;
	Weapon* gun;

	std::vector<glm::vec3> model_position;

	glm::mat4x4 model_matrix[BAR_SIZE];
	glm::vec4 magData[BAR_SIZE];

	float MagazineState(int index);

public:
	HUD();
	virtual ~HUD();

	void setCamera(Camera* cam);
	void setGun(Weapon* gun);	

	virtual bool init();

	virtual bool frame(float delta_time);

	virtual bool draw();


};