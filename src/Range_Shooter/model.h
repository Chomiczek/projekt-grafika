#pragma once

#include "libs.h"



class Model : public Object
{
protected:

	gl::GLuint sampler_handle;
	std::string model_path;
	std::string model_path_name;
	std::string texture_path;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	glm::vec3 model_position = glm::vec3(0.f, 0.f, 0.f);

	glm::mat4x4 model_matrix;

	

	virtual void generateModel(gl::GLuint vertex_position_loction, gl::GLuint vertex_tex_uv_loction, gl::GLuint vertex_normal_loction);
	
	// Prepares model for use. Loads and generates it, sets textures and samplers
	virtual void prepareModel(gl::GLuint vertex_position_loction = 0u, gl::GLuint vertex_tex_uv_loction = 1u, gl::GLuint vertex_normal_loction = 2u);
public:
	Model();
	virtual ~Model();

	
	virtual std::vector < glm::vec3> getColloder();
	virtual glm::vec3  getPosition();
	virtual void setPosition(glm::vec3 pos);

	virtual void setTexture(std::string texture_path);

	virtual void setModelPath(std::string model_path);
	
	virtual void LoadObj(std::string model_path, std::string model_name);

	virtual bool init();

	virtual bool draw();

};