#include "ambientlight.h"

AmbientLight::AmbientLight()
{
	light.color = glm::vec3();
	light.position = glm::vec3();
	light.radius = 0.f;
}

AmbientLight::~AmbientLight()
{
}

bool AmbientLight::setAmbientLight(glm::vec3 color)
{

	ambient_light = color;
	return true;
}



bool AmbientLight::draw()
{
	if (!shaderProgram.use())
	{
		std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
	}

	shaderProgram.updateUniform<glm::vec3>("AmbientLight", &ambient_light, sizeof(glm::vec3));
	shaderProgram.updateUniform<Point_Light>("PointLight", &light, sizeof(Point_Light));
	return true;
}

