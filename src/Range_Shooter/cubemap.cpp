#include "cubemap.h"

Cubemap::Cubemap()
{
}

Cubemap::Cubemap(Shader shader)
{
    setShader(shader);
}

Cubemap::~Cubemap()
{
}

void Cubemap::setCubeTexture(std::string cube_texture_path)
{
    
    gl::glGenTextures(1, &cubemapID);
    gl::glBindTexture(gl::GL_TEXTURE_CUBE_MAP, cubemapID);
    
    cubemapID = loadTexture(cube_texture_path);
    gl::glGenSamplers(1, &sampler_handle);

    gl::glTexParameteri(gl::GL_TEXTURE_CUBE_MAP, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
    gl::glTexParameteri(gl::GL_TEXTURE_CUBE_MAP, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
    gl::glTexParameteri(gl::GL_TEXTURE_CUBE_MAP, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
    gl::glTexParameteri(gl::GL_TEXTURE_CUBE_MAP, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
    gl::glTexParameteri(gl::GL_TEXTURE_CUBE_MAP, gl::GL_TEXTURE_WRAP_R, gl::GL_CLAMP_TO_EDGE);
    
}

bool Cubemap::init()
{
    gl::glEnable(gl::GL_TEXTURE_CUBE_MAP_SEAMLESS);
    gl::glEnable(gl::GL_DEPTH_TEST);
    
    //std::cout << "\t# " << typeid(*this).name() << ": Init ..." << std::endl;
    std::vector<gl::GLfloat> skyboxVertices = {
        -1.0f,  1.0f, -1.0f,	0.0f,1.0f,	0.f,0.f,-1.f,
        -1.0f, -1.0f, -1.0f,	0.0f,0.0f,	0.f,0.f,-1.f,
         1.0f, -1.0f, -1.0f,	1.0f,0.0f,	0.f,0.f,-1.f,
         1.0f, -1.0f, -1.0f,	1.0,0.0f,	0.f,0.f,-1.f,
         1.0f,  1.0f, -1.0f,	1.0f,1.f,	0.f,0.f,-1.f,
        -1.0f,  1.0f, -1.0f,	0.0f,1.0f,	0.f,0.f,-1.f,

        -1.0f, -1.0f,  1.0f,	0.0f,1.0f,	-1.f,0.f,0.f,
        -1.0f, -1.0f, -1.0f,	0.0f,0.0f,	-1.f,0.f,0.f,
        -1.0f,  1.0f, -1.0f,	1.0f,0.0f,	-1.f,0.f,0.f,
        -1.0f,  1.0f, -1.0f,	1.0,0.0f,	-1.f,0.f,0.f,
        -1.0f,  1.0f,  1.0f,	1.0f,1.f,	-1.f,0.f,0.f,
        -1.0f, -1.0f,  1.0f,	0.0f,1.0f,	-1.f,0.f,0.f,

         1.0f, -1.0f, -1.0f,	0.0f,1.0f,	1.f,0.f,0.f,
         1.0f, -1.0f,  1.0f,	0.0f,0.0f,	1.f,0.f,0.f,
         1.0f,  1.0f,  1.0f,	1.0f,0.0f,	1.f,0.f,0.f,
         1.0f,  1.0f,  1.0f,	1.0,0.0f,	1.f,0.f,0.f,
         1.0f,  1.0f, -1.0f,	1.0f,1.f,	1.f,0.f,0.f,
         1.0f, -1.0f, -1.0f,	0.0f,1.0f,	1.f,0.f,0.f,

        -1.0f, -1.0f,  1.0f,	0.0f,1.0f,	0.f,0.f,1.f,
        -1.0f,  1.0f,  1.0f,	0.0f,0.0f,	0.f,0.f,1.f,
         1.0f,  1.0f,  1.0f,	1.0f,0.0f,	0.f,0.f,1.f,
         1.0f,  1.0f,  1.0f,	1.0,0.0f,	0.f,0.f,1.f,
         1.0f, -1.0f,  1.0f,	1.0f,1.f,	0.f,0.f,1.f,
        -1.0f, -1.0f,  1.0f,	0.0f,1.0f,	0.f,0.f,1.f,

        -1.0f,  1.0f, -1.0f,	0.0f,1.0f,	0.f,1.f,0.f,
         1.0f,  1.0f, -1.0f,	0.0f,0.0f,	0.f,1.f,0.f,
         1.0f,  1.0f,  1.0f,	1.0f,0.0f,	0.f,1.f,0.f,
         1.0f,  1.0f,  1.0f,	1.0,0.0f,	0.f,1.f,0.f,
        -1.0f,  1.0f,  1.0f,	1.0f,1.f,	0.f,1.f,0.f,
        -1.0f,  1.0f, -1.0f,	0.0f,1.0f,	0.f,1.f,0.f,

        -1.0f, -1.0f, -1.0f,	0.0f,1.0f,	0.f,-1.f,0.f,
        -1.0f, -1.0f,  1.0f,	0.0f,0.0f,	0.f,-1.f,0.f,
         1.0f, -1.0f, -1.0f,	1.0f,0.0f,	0.f,-1.f,0.f,
         1.0f, -1.0f, -1.0f,	1.0,0.0f,	0.f,-1.f,0.f,
        -1.0f, -1.0f,  1.0f,	1.0f,1.f,	0.f,-1.f,0.f,
         1.0f, -1.0f,  1.0f,	0.0f,1.0f,	0.f,-1.f,0.f
    };
    std::vector<gl::GLushort> skyboxIndices;
    for (int i = 0; i < skyboxVertices.size(); i++) skyboxIndices.push_back(i);
    indicesNumber = skyboxIndices.size();

    const gl::GLuint vertex_position_location = 0u;
    generateObject(skyboxVertices, skyboxIndices, vertex_position_location);
    gl::glBindVertexArray(objectVAO);

    gl::glBindSampler(0, sampler_handle);
    gl::glActiveTexture(gl::GL_TEXTURE0);
    gl::glBindTexture(gl::GL_TEXTURE_CUBE_MAP, cubemapID);
   
    gl::glBindVertexArray(0);
    return true;
}

bool Cubemap::draw()
{
    gl::GLenum data;
    gl::glGetIntegerv(gl::GL_DEPTH_FUNC, &data);
    gl::glDepthRange(1, 1);
    gl::glDepthFunc(gl::GL_LEQUAL);
    if (!shaderProgram.use())
    {
        std::cout << "\t# " << typeid(*this).name() << ": WARNING: Shader Program has not been set ..." << std::endl;
    }

    gl::glBindTexture(gl::GL_TEXTURE_CUBE_MAP, cubemapID);


    gl::glActiveTexture(gl::GL_TEXTURE0);
    gl::glBindTexture(gl::GL_TEXTURE_CUBE_MAP, cubemapID);

    drawOnScreen();
    
    gl::glDepthFunc(data);
    gl::glDepthRange(0.02, 1);
	return true;
}

