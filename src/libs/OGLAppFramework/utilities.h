#pragma once

#include "externallibs.h"
#include <optional>

//#define STB_IMAGE_STATIC
//#define STB_IMAGE_IMPLEMENTATION
//#include "stb_image.h"


namespace OGLA
{
    // funkcja wczytujaca shader-y z plikow, kompilujaca je i tworzaca program
    // przyjmuje na wejscie sciezki do VS i FS
    // w przypadku powodzenia zwraca uchwyt (handle) do programu
    std::optional<gl::GLuint> createProgram(const std::string& vs_source_path, const std::string& fs_source_path);

    // funkcja wczytujaca teksture z pliku (format .dds, lub .ktx), tworzaca "Texture Object" i w przypadku powodzenia zwracająca uchwyt (handle) do niego
    std::optional<gl::GLuint> loadTextureDDS(const std::string& file_path);

    // funckja zwracajaca macierz skalowania
    glm::mat4x4 scaleMatrix(float x, float y, float z);
    // funckja zwracajaca macierz obrotu
    glm::mat4x4 rotationMatrix(float angle, const glm::vec3& axis);
    // funckja zwracajaca macierz translacji
    glm::mat4x4 translationMatrix(const glm::vec3& translation);

    //unsigned char* loadTexture(const char* file_name, int* widith, int* height, int* comp, int req_comp);
}  // namespace OGLAppFramework
